import java.util.*;

public class LongStack {

    private LinkedList<Long> stack;

    public static void main(String[] argum) {
//        System.out.println (interpret("-234"));
//        System.out.println (interpret("     "));
//        System.out.println (interpret("-234 @"));
//        System.out.println (interpret("-234 -"));
//        System.out.println (interpret("-234 56"));
        System.out.println(interpret("-234 56 #"));
    }

    LongStack() {
        stack = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack tmp = new LongStack();
        for (Long element : stack) {
            tmp.stack.addLast(element);
        }
        return tmp;
    }

    public boolean stEmpty() {
        return stack.isEmpty();
    }

    public void push(long a) {
        stack.push(a);
    }

    public long pop() {

        if (!stEmpty()) {
            return stack.pop();
        }
        throw new RuntimeException("Stack is empty!");
    }

    public void op(String s) {
        if (stack.size() > 1) {
            if (isOperator(s)) {
                long ele2 = stack.pop();
                long ele1 = stack.pop();
                stack.push(calculate(ele1, ele2, s));
            } else {
                throw new RuntimeException("Invalid input: " + s);
            }
        } else {
            throw new RuntimeException("Invalid input: " + s + "\nStack has less than two elements!");
        }
    }

    public long tos() {
        if (stEmpty()) {
            throw new RuntimeException("Stack is empty!");
        } else {
            return stack.peekFirst();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != LongStack.class) return false;
        if (((LongStack) o).stack.size() != stack.size())
            return false;
        if (stack.size() > 0) {
            for (int i = 0; i <= stack.size() - 1; i++)
                if (!((LongStack) o).stack.get(i).equals(stack.get(i)))
                    return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = stack.size() - 1; i >= 0; i--) {
            builder.append(stack.get(i).toString()).append(" ");
        }
        return builder.toString();
    }

    public static long interpret(String pol) {
        LongStack stack = new LongStack();
        if (pol == null) {
            throw new RuntimeException("Invalid input! Input is empty!");
        }
        String tmp = pol.replaceAll("[\\n\\t ]", "");
        if ((tmp.length() == 0)) {
            throw new RuntimeException("Invalid input! Input is empty or contains only whitespaces!");
        }

        String[] input = pol.replaceAll("[\\n\\t]", " ").split(" ");
        if (stack.isOperator(input[0])) {
            throw new RuntimeException("Invalid input: " + pol + "\nThere can be no operators before numbers!");
        }

        for (String s : input) {
            if (stack.isLong(s)) {
                stack.push(Long.parseLong(s));
            } else if (stack.isOperator(s)) {
                try {
                    stack.op(s);
                } catch (RuntimeException e) {
                    throw new RuntimeException("Invalid input: " + pol +
                            "\n Method op can not be completed with given elements.");
                }
            } else if (!s.equals(" ") && !s.equals("")) {
                throw new RuntimeException("Invalid input: " + pol
                        + "\nOnly numbers, operators and whitespaces are allowed!");
            }
        }

        if (stack.stack.size() > 1) {
            throw new RuntimeException("Invalid input: " + pol
                    + "\nIncorrect amount of numbers given. " +
                    "Calculation can not be completed with given elements.");
        }
        return stack.tos();
    }

    private long calculate(long ele1, long ele2, String operator) {
        if ("+".equals(operator)) {
            return ele1 + ele2;
        } else if ("-".equals(operator)) {
            return ele1 - ele2;
        } else if ("*".equals(operator)) {
            return ele1 * ele2;
        }
        return ele1 / ele2;
    }

    private boolean isLong(String s) {
        try {
            Long.parseLong(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isOperator(String s) {
        List<String> operators = Arrays.asList("+", "-", "*", "/");
        return (operators.contains(s));
    }
}

